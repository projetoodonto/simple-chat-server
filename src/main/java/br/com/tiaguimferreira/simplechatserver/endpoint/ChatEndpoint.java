/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tiaguimferreira.simplechatserver.endpoint;

import br.com.tiaguimferreira.simplechatserver.bean.ChatMessage;
import br.com.tiaguimferreira.simplechatserver.converters.ChatMessageDecoder;
import br.com.tiaguimferreira.simplechatserver.converters.ChatMessageEncoder;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 *
 * @author tiago
 */
@ServerEndpoint(value = "/chat/{user}", encoders = ChatMessageEncoder.class, decoders = ChatMessageDecoder.class)
public class ChatEndpoint {

    private Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    private List<String> users = new LinkedList<>();


    @OnOpen
    public void onOpem(Session session, @PathParam("user") final String user) {
        System.out.println("Session opem!");
        session.getUserProperties().put("user", user);
//        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
        peers.add(session);
    }

    @OnMessage
    public void onMessage(Session session, final ChatMessage chatMessage) {
        String user = (String) session.getUserProperties().get("user");
        try {
            for (Session ses : peers) {
                if (ses.isOpen()) {
                    ses.getBasicRemote().sendObject(chatMessage);
                }
            }
        } catch (IOException | EncodeException e) {
            e.printStackTrace();
        }
    }

    public void onError(Session session, Throwable t) {
        peers.remove(session);
        t.printStackTrace();
    }

    @OnClose
    public void onClose(Session session) {
        peers.remove(session);
    }
}
