/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tiaguimferreira.simplechatserver.controllers;

import br.com.tiaguimferreira.simplechatserver.bean.ChatMessage;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author tiago
 */
@ManagedBean
@SessionScoped
public class LoginController implements Serializable {

    private static final Long serialVersionUID = 1L;
    private List<ChatMessage> users = new LinkedList<>();
    private ChatMessage chatMessage = new ChatMessage();

    public String login() {
        users.add(chatMessage);
        return "/restrict/home.faces";
    }

    public String logout() {
        return "/login.faces";
    }

    public List<ChatMessage> getUsers() {
        return users;
    }

    public void setUsers(List<ChatMessage> users) {
        this.users = users;
    }
    
    public void addUser(ChatMessage chatMessage){
        users.add(chatMessage);
    }

    public ChatMessage getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(ChatMessage chatMessage) {
        this.chatMessage = chatMessage;
    }

}
