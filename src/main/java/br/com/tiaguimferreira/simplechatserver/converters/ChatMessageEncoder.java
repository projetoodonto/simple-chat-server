/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.tiaguimferreira.simplechatserver.converters;

import br.com.tiaguimferreira.simplechatserver.bean.ChatMessage;
import javax.json.Json;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author tiago
 */
public class ChatMessageEncoder implements Encoder.Text<ChatMessage>{

    @Override
    public String encode(final ChatMessage chatMessage) throws EncodeException {
        return Json.createObjectBuilder()
                .add("date", chatMessage.getDate().toString())
                .add("message", chatMessage.getMessage())
                .add("sender", chatMessage.getSender())
                .add("user", chatMessage.getUser())
                .build()
                .toString();
                
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
    
}
