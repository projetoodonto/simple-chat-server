/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.tiaguimferreira.simplechatserver.converters;

import br.com.tiaguimferreira.simplechatserver.bean.ChatMessage;
import java.io.StringReader;
import java.util.Date;
import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author tiago
 */
public class ChatMessageDecoder implements Decoder.Text<ChatMessage> {
    
    @Override
    public ChatMessage decode(final String message) throws DecodeException {
        ChatMessage chatMessage = new ChatMessage();
        JsonObject obj = Json.createReader(new StringReader(message)).readObject();
        chatMessage.setDate(new Date());
        chatMessage.setMessage(obj.getString("message"));
        chatMessage.setSender(obj.getString("sender"));
        chatMessage.setUser(obj.getString("user"));
        return chatMessage;
    }
    
    @Override
    public boolean willDecode(String arg0) {
        return true;
    }
    
    @Override
    public void init(EndpointConfig config) {
    }
    
    @Override
    public void destroy() {
    }
    
}
